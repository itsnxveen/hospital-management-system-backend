package com.jwt.implementation.service;

import java.sql.Date;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.jwt.implementation.model.Appointment;
import com.jwt.implementation.model.Role;
import com.jwt.implementation.model.TimeSlots;
import com.jwt.implementation.model.User;
import com.jwt.implementation.model.UserDTO;
import com.jwt.implementation.model.doctors;
import com.jwt.implementation.model.doctorsDto;
import com.jwt.implementation.repository.RoleRepository;
import com.jwt.implementation.repository.UserRepository;
import com.jwt.implementation.repository.appointmentRepository;
import com.jwt.implementation.repository.doctorRepository;
import com.jwt.implementation.repository.timeSlotRepository;
import com.jwt.implementation.exception.*;


@Service
public class DefaultUserServiceImpl implements DefaultUserService{

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	RoleRepository roleRepo;
	
	@Autowired
	doctorRepository docRepo;
	
	@Autowired
	appointmentRepository appointmentRepo;
	
    @Autowired
    timeSlotRepository timeRepo;
	
	private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	
	@Autowired
	private JavaMailSender mailSender;
	
	
	public void sendEmail(String fromEmail,String toEmail,String Subject,String body) {
		
		SimpleMailMessage mailMessage=new SimpleMailMessage();
		mailMessage.setFrom(fromEmail);
		mailMessage.setTo(toEmail);
		mailMessage.setSubject(Subject);
		mailMessage.setText(body);
		
		mailSender.send(mailMessage);
	}
	
	
	//throws UsernameNotFoundException
	@Override
	public UserDetails loadUserByUsername(String username)  {
		
		 User user = userRepo.findByUserName(username);
		                                                     
		 if(user==null) {
			 throw new ResourceNotFoundException("user is not exist with given name:"+username);
		 }
	     return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), mapRolesToAuthorities(user.getRole()));
	}
	
	public Collection<? extends GrantedAuthority> mapRolesToAuthorities(Set<Role> roles){
		return roles.stream().map(role -> new SimpleGrantedAuthority(role.getRole())).collect(Collectors.toList());
	}

	@Override
	public User save(UserDTO userRegisteredDTO) {
		Role role = new Role();
		if(userRegisteredDTO.getRole().equals("USER"))
		  role = roleRepo.findByRole("ROLE_USER");
		else if(userRegisteredDTO.getRole().equals("ADMIN"))
		 role = roleRepo.findByRole("ROLE_ADMIN");
		User user = new User();
		user.setEmail(userRegisteredDTO.getEmail());
		user.setUserName(userRegisteredDTO.getUserName());
		user.setPassword(passwordEncoder.encode(userRegisteredDTO.getPassword()));
		user.setRole(role);
		
		return userRepo.save(user);
	}

	@Override
	public doctors addDoctors(doctors doc) {
		
		doctors savedDoc=docRepo.save(doc);
	
		
		return savedDoc;
	}
	
	
	@Override
	public List<doctors> getDoctorsBylocAndDept(doctors doc) {
		         
      return  docRepo.findByLocationAndDepartment(doc.getLocation(),doc.getDepartment());
		  
	}
	
	String name;
	int age;
	Date date;
	String time;
	String datas;
	String gmail;
	String doctorname;
	String hospital;
	int consultFees;
	String status;
    
	@Override
	public Appointment createAppointment(Appointment appointment) {
		
		Appointment savedAppointment= appointmentRepo.save(appointment);
		
		name=savedAppointment.getPatientName();
		age=savedAppointment.getAge();
		date=savedAppointment.getDateAndTime();
		time=savedAppointment.getTime();
		gmail=savedAppointment.getEmail();
		doctorname=savedAppointment.getDoctorName();
		hospital=savedAppointment.getHospitalName();
		consultFees=savedAppointment.getConsulationFees();
		status=savedAppointment.getStatus();
		
		datas="Patient Name :"+name+"\nAge :"+age+"\nAppointment Date :"+date+"\nAppointment Time :"+time+
			  "\nDoctor Name: "+doctorname+"\nHospital Name :"+hospital+"\nconsultFees :"+consultFees+"\nStatus :"+status;
		
		sendEmail("its.nxveen@gmail.com", gmail,"your appointment status",datas);
		  
		return savedAppointment;
	}

	@Override
	public void deleteDoctor(Integer docId) {
		
		docRepo.deleteById(docId);
		
	}

	@Override
	public List<Appointment> getAllappointments() {
		
		List<Appointment> appointments=appointmentRepo.findAll();
		
		return appointments;
	}




	@Override
	public List<doctors> getDoctorsByHospital(doctors doc) {

		             		
		  List<doctors> doctorsDetails=docRepo.findByHospitalName(doc.getHospitalName());
		     
		  return doctorsDetails;
	}




	@Override
	public List<Appointment> getAppointmentByEmail(Appointment appo) {
		
		List<Appointment> appointments=appointmentRepo.findByEmail(appo.getEmail());    
		
		return appointments;
	}




	@Override
	public void deleteappointmentById(Integer id) {
		
	 appointmentRepo.deleteById(id);
		
	}


	@Override
	public TimeSlots addTimeSlotsById(TimeSlots timeslot) {
		
		return timeRepo.save(timeslot);
	}



	@Override
	public List<Appointment> getAppointmentByHospital(Appointment appoint) {
		
		List<Appointment> appointments=appointmentRepo.findByHospitalName(appoint.getHospitalName());
		
		return appointments;
	}

	
    String currentStatus;
    String toEmail;
	@Override
	public Appointment updateAppointments(Integer appointmentId, Appointment updatedAppointment) {
		
		  Appointment  appoint=appointmentRepo.findAllById(appointmentId);
		  
		  appoint.setStatus(updatedAppointment.getStatus());
		  
		  currentStatus=appoint.getStatus();
		  
	      toEmail=appoint.getEmail();
	      
		  sendEmail("its.nxveen@gmail.com", toEmail,"your appointment status",currentStatus);
		
		  return appointmentRepo.save(appoint);
	}



//changes
	
	
}
