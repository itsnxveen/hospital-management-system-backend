package com.jwt.implementation.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;


import com.jwt.implementation.model.Appointment;
import com.jwt.implementation.model.TimeSlots;
import com.jwt.implementation.model.User;
import com.jwt.implementation.model.UserDTO;
import com.jwt.implementation.model.doctors;
import com.jwt.implementation.model.doctorsDto;

public interface DefaultUserService extends UserDetailsService{
	
	User save(UserDTO userRegisteredDTO);
	
	doctors addDoctors(doctors doc);

	List<doctors> getDoctorsBylocAndDept(doctors doc);

	Appointment createAppointment(Appointment appointment);

	void deleteDoctor(Integer docId);

	List<Appointment> getAllappointments();

	List<doctors> getDoctorsByHospital(doctors doc);

	List<Appointment> getAppointmentByEmail(Appointment appo);

	void deleteappointmentById(Integer id);

	TimeSlots addTimeSlotsById(TimeSlots timeslot);

	List<Appointment> getAppointmentByHospital(Appointment appoint);

	Appointment updateAppointments(Integer appointmentId, Appointment updatedAppointment);


}
