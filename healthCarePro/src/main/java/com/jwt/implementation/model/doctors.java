package com.jwt.implementation.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity

@Table(name="doctors")
public class doctors {
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int doctorId;
	
	private String location;
	private String department;
	private String doctorName;
	private String hospitalName;
	private int consulationFees;
	
    @ManyToMany(fetch=FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name="doctors_timeslot",
    
    		joinColumns = {
    				@JoinColumn(name="DOCTOR_ID")
    		},
    		inverseJoinColumns = {
    	           @JoinColumn(name="TIME_ID")
    		}
    		 )
	private List<TimeSlots> timeslot;
    
	public int getDoctorId() {
		return doctorId;
	}
	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public int getConsulationFees() {
		return consulationFees;
	}
	public void setConsulationFees(int consulationFees) {
		this.consulationFees = consulationFees;
	}
	
//	
//	public List<TimeSlots> getTimeslot() {
//		return timeslot;
//	}
//	public void setTimeslot(List<TimeSlots> timeslot) {
//		this.timeslot = timeslot;
//	}
	public doctors(int doctorId, String location, String department, String doctorName, String hospitalName,
			int consulationFees,List<TimeSlots> timeslot) {
		super();
		this.doctorId = doctorId;
		this.location = location;
		this.department = department;
		this.doctorName = doctorName;
		this.hospitalName = hospitalName;
		this.consulationFees = consulationFees;
	    this.timeslot=timeslot;
	}
	public doctors() {
		
	}
	
	
}
