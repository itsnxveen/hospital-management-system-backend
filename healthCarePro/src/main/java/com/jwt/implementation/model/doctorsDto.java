package com.jwt.implementation.model;

import java.util.List;

public class doctorsDto {

	private int doctorId;
	private String location;
	private String department;
	private String doctorName;
	private String hospitalName;
	private int consulationFees;
	
	private List<TimeSlots> timeSlots;
	
	public int getDoctorId() {
		return doctorId;
	}
	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public int getConsulationFees() {
		return consulationFees;
	}
	public void setConsulationFees(int consulationFees) {
		this.consulationFees = consulationFees;
	}
	public doctorsDto(int doctorId, String location, String department, String doctorName, String hospitalName,
			int consulationFees,List<TimeSlots> timeSlots) {
		super();
		this.doctorId = doctorId;
		this.location = location;
		this.department = department;
		this.doctorName = doctorName;
		this.hospitalName = hospitalName;
		this.consulationFees = consulationFees;
		this.timeSlots = timeSlots;
	}
	
	
	
	
	
	
}
