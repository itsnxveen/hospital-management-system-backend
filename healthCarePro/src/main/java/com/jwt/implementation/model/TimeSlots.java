package com.jwt.implementation.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="timeslot")
public class TimeSlots {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer timeId;
	
	private String availableTime;
	
	private boolean isAvailable;
	

	public Integer getTimeId() {
		return timeId;
	}

	public void setTimeId(Integer timeId) {
		this.timeId = timeId;
	}

	public String getAvailableTime() {
		return availableTime;
	}

	public void setAvailableTime(String availableTime) {
		this.availableTime = availableTime;
	}

	
	public boolean isAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public TimeSlots(Integer timeId, String availableTime, boolean isAvailable) {
		super();
		this.timeId = timeId;
	
		this.availableTime = availableTime;
		this.isAvailable = isAvailable;
	}

	public TimeSlots() {
		
	}
	
	
}
