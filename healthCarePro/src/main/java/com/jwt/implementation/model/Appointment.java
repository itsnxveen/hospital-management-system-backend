package com.jwt.implementation.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="appointment")
public class Appointment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String patientName;
	private Integer age;
	private String email;
	private Date dateAndTime;
	private String time;
	private String doctorName;
	private String hospitalName;
	private Integer consulationFees;
	private String status;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Date getDateAndTime() {
		return dateAndTime;
	}
	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public Integer getConsulationFees() {
		return consulationFees;
	}
	public void setConsulationFees(Integer consulationFees) {
		this.consulationFees = consulationFees;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Appointment(Integer id, String patientName, Integer age, String email, Date dateAndTime, String time,
			String doctorName, String hospitalName, Integer consulationFees, String status) {
		super();
		this.id = id;
		this.patientName = patientName;
		this.age = age;
		this.email = email;
		this.dateAndTime = dateAndTime;
		this.time = time;
		this.doctorName = doctorName;
		this.hospitalName = hospitalName;
		this.consulationFees = consulationFees;
		this.status = status;
	}
	public Appointment() {
		
	}
	

}
