package com.jwt.implementation.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.jwt.implementation.config.JwtGeneratorValidator;
import com.jwt.implementation.model.Appointment;
import com.jwt.implementation.model.TimeSlots;
import com.jwt.implementation.model.User;
import com.jwt.implementation.model.UserDTO;
import com.jwt.implementation.model.doctors;
import com.jwt.implementation.model.doctorsDto;
import com.jwt.implementation.repository.UserRepository;
import com.jwt.implementation.service.DefaultUserService;


@RestController

@CrossOrigin(origins = "http://localhost:4200")
public class RestAppController {

	@Autowired
	UserRepository userRepo;
	

	@Autowired
	AuthenticationManager authManager;

	@Autowired
	JwtGeneratorValidator jwtGenVal;
	
	@Autowired
	BCryptPasswordEncoder bcCryptPasswordEncoder;
	
	@Autowired
	DefaultUserService userService;

	@PostMapping("/registration")
	public ResponseEntity<Object> registerUser(@RequestBody UserDTO userDto) {
		User users =  userService.save(userDto);
		if (users.equals(null))
			return generateRespose("Not able to save user ", HttpStatus.BAD_REQUEST, userDto);
		else
			return generateRespose("User saved successfully : " + users.getId(), HttpStatus.OK, users);
	}

	@PostMapping("/genToken")
	public String generateJwtToken(@RequestBody UserDTO userDto) throws Exception {
		
			Authentication authentication = authManager.authenticate(
			new UsernamePasswordAuthenticationToken(userDto.getUserName(), userDto.getPassword()));
			SecurityContextHolder.getContext().setAuthentication(authentication);
		
		return jwtGenVal.generateToken(authentication);
	}
   
	@PostMapping("/addDoctor")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public ResponseEntity<doctors> addDoctorsToTable(@RequestBody doctors doc) {
		
		doctors Doc=userService.addDoctors(doc);
		
		return new ResponseEntity<>(Doc,HttpStatus.CREATED);
	}
	 
	
    @PostMapping("/search")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<List<doctors>> searchDoctors(@RequestBody doctors doc) {
        		
    	List<doctors> getDoc=userService.getDoctorsBylocAndDept(doc);


        return new ResponseEntity(getDoc,HttpStatus.OK);
    }
    
    @PostMapping("/booking")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public Appointment bookAppointment(@RequestBody Appointment appointment) {
		
    	Appointment appoint=userService.createAppointment(appointment);
    	
    	return appoint;
    }
    
    
    @DeleteMapping({"/deleteDoc/{id}"})
    @PreAuthorize("hasAuthority('ROLE_USER')")
    
    public String deleteDoctors(@PathVariable("id") Integer docId) {
    	
        userService.deleteDoctor(docId);
    	
    	return "Doctor profile deleted successfully";
    }
    
    
    
    
    @GetMapping("/getAppointments")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public List<Appointment> getAppointmentDetails() {
    	
    	
    	return userService.getAllappointments();
    }
    
    @PostMapping("/getByHospital")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<List<doctors>> getDoctorsByHospitals(@RequestBody doctors doc) {
    	
    	List<doctors>  doctorsByHospital=userService.getDoctorsByHospital(doc);
    	
        
    	return new ResponseEntity(doctorsByHospital,HttpStatus.OK) ;
    }
    
    @PostMapping("/getByEmail")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public List<Appointment> getAppointment(@RequestBody Appointment appo ) {
    	
    	return userService.getAppointmentByEmail(appo);	
    	
    }
    
    
    @DeleteMapping("/deleteById/{id}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public String deleteAppointment(@PathVariable("id") Integer id) {
    	userService.deleteappointmentById(id);
    	return "deleted succesfully";
    }
    
    @DeleteMapping("/deleteByAdmin/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteAppointmentByAdmin(@PathVariable("id") Integer id) {
    	userService.deleteappointmentById(id);
    	return "deleted succesfully";
    }
    
    
	public ResponseEntity<Object> generateRespose(String message, HttpStatus st, Object responseobj) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("meaasge", message);
		map.put("Status", st.value());
		map.put("data", responseobj);
		 map.put("Access-Control-Allow-Origin", "*");
		return new ResponseEntity<Object>(map, st);
	}
	
	@PostMapping("/addTime")
 
	public TimeSlots addTimeslot(@RequestBody TimeSlots timeslot) {
		
		return userService.addTimeSlotsById(timeslot);
			
	}
	
	@PostMapping("/getByHospitalName")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public List<Appointment> getAppointmentByHospitalName(@RequestBody Appointment appoint) {
		
		return userService.getAppointmentByHospital(appoint);
	}
	
	
	@PutMapping("/updateStatus/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Appointment updateAppointmentById(@PathVariable("id") Integer appointmentId,@RequestBody Appointment updatedAppointment) {
		
		
		return userService.updateAppointments(appointmentId,updatedAppointment);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
//    public static doctorsDto mapTodoctorsDto(doctors doc) {
//		
//		
//		return new doctorsDto(
//				doc.getDoctorId(),
//				doc.getLocation(), 
//				doc.getDepartment(), 
//				doc.getDoctorName(), 
//				doc.getHospitalName(),
//				doc.getConsulationFees()
//				
//				);
//		
//	}
//	List<doctorsDto> getDtoDoc=getDoc.stream().map(s->mapTodoctorsDto(s)).toList();
//naveen S

}
