//package com.jwt.implementation.mapper;
//
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import org.mapstruct.factory.Mappers;
//
//import com.jwt.implementation.model.doctors;
//import com.jwt.implementation.model.doctorsDto;
//
//public interface doctorMapper {
//
//	@Mapper
//	public interface DoctorMapper {
//		
//		DoctorMapper INSTANCE = Mappers.getMapper(DoctorMapper.class);
//
//	    @Mapping(target = "timeSlots", ignore = true)
//	    doctors mapToEntity(doctorsDto doctorDTO);
//
//	    doctorsDto mapToDTO(doctors doctor);
//	}
//}
