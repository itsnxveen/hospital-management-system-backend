package com.jwt.implementation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jwt.implementation.model.doctors;

public interface doctorRepository extends JpaRepository<doctors, Integer>{

	List<doctors> findByLocationAndDepartment(String location, String department);
		
	List<doctors> findByHospitalName(String hospitalName);

}
