package com.jwt.implementation.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jwt.implementation.model.Appointment;
import java.util.List;


public interface appointmentRepository extends JpaRepository<Appointment, Integer>{
	

    List<Appointment> findByEmail(String email); 
    
    List<Appointment> findByHospitalName(String hospitalName);

	Appointment findAllById(Integer appointmentId);

}
